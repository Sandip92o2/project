<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookTableRequest;
use App\Models\BookTable;
use Exception;
use Illuminate\Http\Request;

class BookTableController extends Controller
{
    public function index()
    {
        $tables=BookTable::all();
     
        return view('backend.booking_t.index', compact('tables'));
    }

    
    public function show($id)
    {
        $table=BookTable::find($id);
        return view('backend.booking_t.show', compact('table'));
    }

    public function create()
    {
        return view('backend.booking_t.create');
    }

    public function insert(Request $request)
    {
      
            $data=$request->all();
         
            BookTable::create($data);

            return redirect()->back()->withMessage('Success Booking');

       
    }

    public function delete($id)
    {
        $booktable=BookTable::find($id);
        $booktable->delete();
        return redirect()->back()->withMessage('Deleted Success');
    }
}

