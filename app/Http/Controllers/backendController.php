<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;

class backendController extends Controller
{
    public function dashboard(){
        return view('backend.dashboard');
    }

    public function search(Request $request)
    {
        $search=$request->search;
        $orders=Order::where('name', 'like', '%'.$search.'%')->orwhere('foodname', 'like', '%'.$search.'%')
        ->get();
       
        return view('backend.orders.index', compact('orders'));
    }

}
