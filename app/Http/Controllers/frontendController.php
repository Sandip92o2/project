<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\chef;
use App\Models\Item;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class frontendController extends Controller
{
    public function home()
    {
        // url 
        if(Auth::id())
        {
            return redirect('redirects');

        }
        else
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        $items = Item::all();
        $categories = Category::all();
        $chefs=chef::all();
        return view('frontend.home', compact('items', 'categories', 'chefs', 'count'));
    }

    public function redirects()
    {
        $items = Item::all();
        $categories = Category::all();
        $chefs = chef::all();
        $usertype= Auth::user()->user_type_id;
        if($usertype=='1')
        {
            
            return view('backend.dashboard');
        }
        else{
            // cart here...
            $user_id=Auth::id();
            $count=Cart::where('user_id', $user_id)->count();
            return view('frontend.home', compact('items', 'categories', 'chefs', 'count'));
        }
    }

    public function about()
    {
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        return view('frontend.about', compact('count'));
    }

    public function service()
    {
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        return view('frontend.service', compact('count'));
    }

    public function menu()
    {
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        $items = Item::all();
        $categories = Category::all();
        return view('frontend.menu', compact('count', 'items', 'categories'));
    }

    public function team()
    {
        $chefs= chef::all();
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        return view('frontend.team', compact('count', 'chefs'));
    }


    public function contact()
    {
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        return view('frontend.contact', compact('count'));
    }

    public function tablebooking()
    {
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        return view('frontend.tablebooking', compact('count'));
    }

    public function categoryWiseItem($id)
    {
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        $category = Category::find($id);
       
        return view('frontend.cat_item', compact('category','count'));
    }

    
    public function showcart(Request $request, $id)
    {
        // dont get another id
        if(Auth::id()==$id)
        {
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        // delete cart and items data
        $data=Cart::select('*')->where('user_id', '=', $id)->get();
        // join here item table and cart table
        $items=Cart::where('user_id', $user_id)->join('items', 'carts.item_id', '=', 'items.id',)->get();
        return view('frontend.cart', compact('count', 'items', 'data'));
        }
        else{
            return redirect()->back();
        }
    }

    // delete carts
    public function delete($id)
    {
        $cart=Cart::find($id);
        $cart->delete();
        return redirect()->back();
    }

    public function item_details($id)
    {
      
        $user_id=Auth::id();
        $count=Cart::where('user_id', $user_id)->count();
        $items = Item::find([$id]);
       
        return view('frontend.itemdetails', [
            'count' =>$count,
            'items' => $items,
            
           
        ]);

    }



}

