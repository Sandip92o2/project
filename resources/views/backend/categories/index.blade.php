@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Category List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href="{{route('category_create')}}"><i class="bi bi-plus-lg"></i>Add new category</a>
            <a class="btn btn-sm btn-danger" href="{{route('category_trashlist')}}"><i class="bi bi-trash"></i><span>{{$deleteItem ?? '0'}}</span> Trash List </a>
            <a class="btn btn-sm btn-success" href=""><i class="bi bi-file-earmark-pdf"></i></i> Download Pdf</a>
            <a class="btn btn-sm btn-dark" href=""><i class="bi bi-file-earmark-excel"></i> Download Excel</a>

            {{-- strat table --}}
            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Seriol Num</th>
                    <th scope="col">Name</th>
                    <th scope="col">Items</th>
                    <th scope="col">Description</th>
                    <th scope="col">image</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($categories as $category)
                  
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$category->name ?? 'no name'}}</td>
                        <td>
                          @foreach ($category->items as $item)
                            <li>
                              {{$item->name}}
                            </li>
                          @endforeach
                        </td>
                        <td>{{$category->description ?? 'no description'}}</td>
                        {{-- image show here --}}
                        <td style="width: 140px">
                            @if(file_exists(storage_path().'/app/public/categories/'.$category->image ) && (!is_null($category->image)))


                               <img src="{{ asset('storage/categories/'. $category->image) }}" width="150" height="100">

                            @else
                               <img src="{{ asset('default.png') }}" height="100" width="150">

                            @endif
                        </td>

                      
                        <td>
                        <a href="{{route('category_show', $category->id)}}" class="btn btn-sm btn-primary"><i class="bi bi-eye"></i>Show</a>
                        <a href="{{route('category_edit', $category->id)}}" class="btn btn-sm btn-warning"><i class="bi bi-pencil"></i>Edit</a>
                        <a href="{{route('category_delete', $category->id)}}" class="btn btn-sm btn-danger"><i class="bi bi-trash3"></i>Delete</a>
                        </td>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection