@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Trush List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href="{{route('item_index')}}"><i class="bi bi-skip-backward-fill"></i> Back</a>
            {{-- strat table --}}
            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Seriol Num</th>
                    <th scope="col">Category_id</th>
                    <th scope="col">Name</th>
                    <th scope="col">image</th>
                    <th scope="col">Price</th>
                    <th scope="col">Discount</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($items as $item)
                    <tr>
                      <th scope="row" class="text-center">{{$i++}}</th>
                      <td>{{$item->category_id ?? 'no id'}}</td>
                      <td>{{$item->name ?? 'no name'}}</td>
                      <td>{{$item->image ?? 'no image'}}</td>
                      <td>{{$item->price ?? 'no price'}}</td>
                      <td>{{$item->discount ?? 'no discount'}}</td>
                        <td>
                          <a href="{{route('item_restore', $item->id)}}" class="btn btn-sm btn-primary"><i class="bi bi-recycle"></i> Restore</a>
                        </td>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection