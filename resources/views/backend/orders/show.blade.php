@extends('backend.master')

@section('main_content')

<div class="card">
    <div class="card-body">
    <h5 class="card-title">Data Show:</h5>

    <p class="card-text"><i><b>User Id:</b></i> {{$order->user_id ?? 'no user id'}}</p>
    <p class="card-text"><i><b>Item Id:</b></i> {{$order->item_id ?? 'no item id'}}</p>
    <p class="card-text"><i><b>Orders Quantity:</b></i> {{$order->quantity ?? 'no quantity'}}</p>
    <p class="card-text"><i><b>Orders Unit Price:</b></i> {{$order->unit_price ?? 'no unit price'}}</p>
    <p class="card-text"><i><b>Orders Total Price:</b></i> {{$order->total_price ?? 'no total price'}}</p>
    <p class="card-text"><i><b>Address:</b></i> {{$order->address ?? 'no total price'}}</p>
    <p class="card-text"><i><b>Phone Number:</b> </i>{{$order->Phone_num ?? 'no total price'}}</p>
    <p class="card-text"><i><b>Email:</b></i> {{$order->email ?? 'no total price'}}</p>
    </div>
  </div>

@endsection
