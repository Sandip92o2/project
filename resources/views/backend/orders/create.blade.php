@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Order Create</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('order_insert')}}" method="POST">
                {{-- token --}}
                @csrf
                {{-- end token --}}
                    <div class="div">
                        <label>Quantity:</label>
                        <input type="number" name="quantity"  value="{{old('quantity')}}"  placeholder="" class="form-control">
                    </div>
            
                    <div class="div">
                        <label>Unit_price:</label>
                        <input type="number"  name="unit_price" value="{{old('unit_price')}}" class="form-control">
                    </div>
                    {{-- validation error --}}
                    @error('unit_price')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                    {{-- end validation error --}}
                    
                    <div class="div">
                        <label>Total_price:</label>
                        <input type="number"  name="total_price" value="{{old('total_price')}}" class="form-control">
                    </div>
                    {{-- validation error --}}
                    @error('total_price')
                        <span class="text-danger">{{ $message }}</span>
                     @enderror
                     {{-- end validation error --}}
                
                     <div class="div">
                        <label>Address:</label>
                        <input type="text"  name="address" value="{{old('address')}}" class="form-control">
                    </div>

                    {{-- validation error --}}
                    @error('address')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                    {{-- end validation error --}}

                    <div class="div">
                        <label>Phone Num:</label>
                        <input type="number"  name="Phone_num" value="{{old('Phone_num')}}" class="form-control">
                    </div>

                    {{-- validation error --}}
                    @error('Phone_num')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                    {{-- end validation error --}}

                    <div class="div">
                        <label>Email Address:</label>
                        <input type="email"  name="email" value="{{old('email')}}" class="form-control">
                    </div>

                    {{-- validation error --}}
                    @error('email')
                    <span class="text-danger">{{ $message }}</span>
                    @enderror
                    {{-- end validation error --}}
                <div>
                    <div><button type="submit" class="btn btn-sm btn-primary m-3">Save</button></div>
                </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>



@endsection
