@extends('backend.master')

@section('main_content')

<div class="container">

    <div class="card shadow">
        <div class="card-header bg-primary-subtle text-emphasis-primary"><h4>Create Comment</h4></div>

        <div class="card-body p-2">
            {{-- start form --}}
            <form action="{{route('comment_insert')}}" method="POST">
                {{-- token --}}
                @csrf
                {{-- end token --}}
                    <div class="div">
                        <label>Comment:</label>
                        <textarea type="text" name="comment"  value=""  placeholder="" class="form-control">
                            {{old('comment')}}
                        </textarea>
                    </div>
                <div>
                    <div><button type="submit" class="btn btn-sm btn-primary m-3">Save</button></div>
                </div>
            </form>
            {{-- end form --}}
        </div>
    </div>
</div>



@endsection
