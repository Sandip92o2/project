@extends('backend.master')

@section('main_content')

{{-- Start Success Message --}}
@if (Session::has('message'))
<P class="alert alert-success m-2">{{Session::get('message')}}</P>
@endif
{{-- end success Message --}}

{{-- table container --}}
<div class="container">
    <div class="card shadow">

        <div class="card-header bg-info text-light ">
          <h4> Comment List</h4>
        </div>
        
        <div class="card-body p-3">
            <a class="btn btn-sm btn-primary" href=""><i class="bi bi-skip-backward"></i> Back</a>
            {{-- strat table --}}
            <table class="table table-sm table-bordered mt-3">
                <thead>
                  <tr class="table-dark text-white">
                    <th scope="col">Seriol Num</th>
                    <th scope="col">User id</th>
                    <th scope="col">Item id</th>
                    <th scope="col">Comment</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                {{-- start php --}}
                @php
                    $i=1
                @endphp
                {{-- end php --}}

                {{-- Start foreach  --}}
                @foreach ($comments as $comment)
                    <tr>
                        <th scope="row" class="text-center">{{$i++}}</th>
                        <td>{{$comment->user_id ?? 'no user id'}}</td>
                        <td>{{$comment->_id ?? 'no item id'}}</td>
                        <td>{{$comment->comment ?? 'no comment'}}</td>
                        <td>
                        <a href="{{route('comment_restore', $comment->id)}}" class="btn btn-sm btn-primary"><i class="bi bi-recycle"></i> Restore</a>
                       
                        </td>
                    </tr>
                @endforeach
                {{-- End foreach--}}
                </tbody>
              </table>
              {{-- end table --}}
        </div>
    </div>
 </div>

@endsection