@extends('backend.master')

@section('main_content')

<div class="card">
    <div class="card-body">
    <h5 class="card-title">Data Show:</h5>

    <p class="card-text"><i><b>User Id:</b></i> {{$table->user_id ?? 'no user id'}}</p>
    <p class="card-text"><i><b>Name:</b></i> {{$table->name ?? 'no name'}}</p>
    <p class="card-text"><i><b>Email:</b></i> {{$table->email ?? 'no email'}}</p>
    <p class="card-text"><i><b>Date & Time:</b></i> {{$table->d_o_t ?? 'no date of birth'}}</p>
    <p class="card-text"><i><b>Number of People:</b></i> {{$table->n_o_p ?? 'no number of people'}}</p>
    <p class="card-text"><i><b>special Request:</b></i> {{$table->special_rq ?? 'no special request'}}</p>
    </div>
  </div>

@endsection
