<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="index.html">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Categories</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('category_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('category_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->

      
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#items-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Items</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="items-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('item_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('item_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->

      
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#sizes-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Sizes</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="sizes-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('size_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('size_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->


      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#cards-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Carts</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="cards-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('cart_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('cart_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#orders-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Orders</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="orders-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('order_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('order_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#bookings-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Table Bookings</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="bookings-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('t_booking_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('t_booking_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#comments-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Comments</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="comments-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('comment_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('comment_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->



      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#users-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Users</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="users-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('user_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#chefs-nav" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span>Chefs</span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="chefs-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
          <li>
            <a href="{{route('chefs_index')}}">
              <i class="bi bi-circle"></i><span>lists</span>
            </a>
          </li>
          <li>
            <a href="{{route('chefs_create')}}">
              <i class="bi bi-circle"></i><span>Create</span>
            </a>
          </li>
        </ul>
      </li><!-- End Components Nav -->


    </ul>

  </aside>