


<div class="container-xxl position-relative p-0">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark px-4 px-lg-5 py-3 py-lg-0">
        <a href="" class="navbar-brand p-0">
            <h1 class="text-primary m-0"><i class="fa fa-utensils me-3"></i>Resturent</h1>
            <!-- <img src="{{ asset('ui/frontend') }}/img/logo.png" alt="Logo"> -->
        </a>

        {{-- Minimize Menu --}}
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="fa fa-bars"></span>
        </button>


        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav ms-auto py-0 pe-4">
                <a href="{{route('front_home')}}" class="nav-item nav-link active">Home</a>
                <a href="{{route('front_about')}}" class="nav-item nav-link">About</a>
                <a href="{{route('front_service')}}" class="nav-item nav-link">Service</a>
                <a href="{{route('front_menu')}}" class="nav-item nav-link">Menu</a>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Pages</a>
                    <div class="dropdown-menu m-0">
                        <a href="{{route('front_tble_booking')}}" class="dropdown-item">Booking</a>
                        <a href="{{route('front_team')}}" class="dropdown-item">Our Team</a>
                    </div>
                </div>
                <a href="{{route('front_tble_booking')}}" class=" nav-item nav-link ">Book A Table</a>
                <a href="{{route('front_contact')}}" class="nav-item nav-link">Contact</a>
                <a href="" class="nav-item nav-link">
                {{-- user cart login here --}}
                @auth
                <a href="{{route('front_cart', Auth::user()->id)}}" class="nav-link text-light">
                   CART[{{$count}}]
                </a>
                @endauth
                {{-- guest cart login here --}}
                @guest
                    Cart[0]
                @endguest
                </a>
            </div>
        </div>
        {{-- forntend login/register here --}}
        <div class="ms-4">
            @if (Route::has('login'))
                <div class="sm:fixed sm:top-0 sm:right-0 p-6 text-right z-10">
                    @auth
                    <x-app-layout>

                    </x-app-layout>
                    
                    @else
                        <a href="{{ route('login') }}" class="text-light m-3 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Log in</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="text-light  m-3 font-semibold text-gray-600 hover:text-gray-900 dark:text-gray-400 dark:hover:text-white focus:outline focus:outline-2 focus:rounded-sm focus:outline-red-500">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </div>
         {{-- forntend login/register here --}}
    </nav>

    
</div>